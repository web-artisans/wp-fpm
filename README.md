# Web Artisans WP-FPM Image #

Base image for WordPress applications being deployed with PHP-FPM. This image does not include a web server. We recommend using it in conjunction with our [Nginx WordPress Reverse Proxy](https://bitbucket.org/web-artisans/nginx-wp) image.