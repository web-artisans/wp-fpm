FROM php:7-fpm

ADD ./php.ini /usr/local/etc/php/

# Install required/useful PHP extensions
# OpenSSL, PDO and Tokenizer are included by default in the php:7-fpm Dockerfile.

RUN apt-get update
RUN apt-get install -y wget libmcrypt-dev libcurl4-openssl-dev libbz2-dev zlib1g-dev libpq-dev
RUN docker-php-ext-install -j$(nproc) bz2 curl mcrypt mbstring pdo_pgsql pdo_mysql json zip

# GIT is required for the install
RUN apt-get install -y git

# Set Up App Dir
RUN mkdir /app
WORKDIR /app

# Content Dir as volume
VOLUME /app/wp-content

# Copy the rest of the application
ONBUILD ADD . /app/